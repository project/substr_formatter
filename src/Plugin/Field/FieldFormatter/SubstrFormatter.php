<?php

namespace Drupal\substr_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'substring' formatter.
 *
 * @FieldFormatter(
 *   id = "substr_formatter",
 *   label = @Translation("Substring Trimming"),
 *   field_types = {
 *     "string"
 *   },
 *   edit = {
 *     "editor" = "form"
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class SubstrFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'trim_offset' => 2,
      'trim_length' => NULL,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['trim_offset'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Trim Offset Position'),
      '#description'   => $this->t('A negative value will trim all characters from the start of the string until the specified character count from the end of the string (trimming from the left), a positive value will do the opposite and trim from the right. For the string "abcdef", 2 would output "cdef" and -2 would output "ef".'),
      '#default_value' => $this->getSetting('trim_offset'),
    ];

    $element['trim_length'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Trimmed String Length (Optional)'),
      '#description'   => $this->t('Sets the string length to a fixed number of characters. For the string "abcdef", with a start/offset position of 2, a trim length of 1 would output "c" and a trim length of -1 would output "cde".'),
      '#default_value' => $this->getSetting('trim_length'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();

    if (!empty($settings['trim_offset']) and !empty($settings['trim_length'])) {
      $summary[] = $this->formatPlural($settings['trim_length'],
        'Display only @trim_length character from the offset position of @trim_offset in the string.',
        'Display only @trim_length characters from the offset position of @trim_offset in the string.',
        ['@trim_offset' => $settings['trim_offset'], '@trim_length' => $settings['trim_length']],
      );
    }
    elseif (!empty($settings['trim_offset']) and empty($settings['trim_length'])) {
      $summary[] = $this->t(
        'Display all characters from the offset position of @trim_offset in the string.',
        ['@trim_offset' => $settings['trim_offset']],
      );
    }
    else {
      $summary[] = $this->t('No trimming will occur.');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $trimStart = (int) $this->getSetting('trim_offset');
    $trimLength = (int) $this->getSetting('trim_length');

    foreach ($items as $delta => $item) {

      $value = $item->value;
      if (empty($value)) {
        $value = $item->getValue();
      }

      if (empty($trimLength)) {
        $elements[$delta] = [
          '#markup' => substr($value, $trimStart),
        ];
      }
      else {
        $elements[$delta] = [
          '#markup' => substr($value, $trimStart, $trimLength),
        ];
      }

    }

    return $elements;
  }

}
