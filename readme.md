# Substring Formatter

## About

I hadn't found a good way to trim from the left using the Drupal interface. The 
PHP [substr](https://www.php.net/manual/en/function.substr.php) function seemed
a good way to go about it, and there's some extra functionality to boot! For
more advanced functionality and examples, read the official PHP documentation
linked above.

As a field formatter you're able to use this in views, content displays, etc.

## Usage

There are two input fields, the first is the `offset` parameter and the second
 `length`.

###  Offset Parameter

A negative value will trim all characters from the start of the string until 
the specified character count from the end of the string (trimming from the 
left), a positive value will do the opposite and trim from the right. For the
 string "abcdef", 2 would output "cdef" and -2 would output "ef"'.

 ### Length Parameter

Sets the string length to a fixed number of characters. For the string "abcdef"
, with a start/offset position of 2, a trim length of 1 would output "c" and a 
trim length of -1 would output "cde".